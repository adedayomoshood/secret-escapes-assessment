import {
  Box,
  Button,
  Flex,
  Heading,
  Image,
  Link,
  Skeleton, Text,
  useDisclosure
} from "@chakra-ui/react";
import { Link as RRLink } from "react-router-dom";

type SearchResultDestinationType = {
  id: string;
  title?: string;
  destination: string;
  LeadPhotoUrl?: string;
};

export const SearchResultDestination = ({
  id,
  title,
  destination,
  LeadPhotoUrl,
}: SearchResultDestinationType) => {
  const { isOpen: isImageLoaded, onOpen: onImageLoaded } = useDisclosure();

  return (
    <Link
      as={RRLink}
      display="flex"
      to={`/sale/${id}`}
      borderRadius="base"
      overflow="hidden"
      flexDir="column"
      boxShadow="rgb(0 0 0 / 25%) 0px 2px 4px 0px"
      _hover={{
        boxShadow: "rgb(0 0 0 / 25%) 0px 8px 16px 0px",
      }}
    >
      <Skeleton isLoaded={isImageLoaded} h={275}>
        <Image
          alt={title}
          loading="lazy"
          src={LeadPhotoUrl}
          onLoad={onImageLoaded}
          objectFit="cover"
          maxW="unset"
          w="100%"
          h={275}
        />
      </Skeleton>

      <Flex flexDir="column" gap={4} p={6} flex={1}>
        <Heading as="h2" size="md" fontWeight="semibold">
          {title}
        </Heading>
        
        <Text fontSize="lg">{destination}</Text>

        <Box mt="auto">
          <Button colorScheme="orange" borderRadius={0} px={6}>
            View Details
          </Button>
        </Box>
      </Flex>
    </Link>
  );
};
