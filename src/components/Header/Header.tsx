import { Box, Container, Flex, Link } from "@chakra-ui/react";
import { Link as RRLink } from "react-router-dom";
import { Logo } from "../Logo/Logo";
import { Search } from "../Search";

export const Header = () => {
  return (
    <Flex
      py={6}
      px={{base:0, lg:6}}
      bg="black"
      justifyContent="space-between"
      pos="sticky"
      zIndex={2}
      top={0}
    >
      <Container
        display="flex"
        gap={{ base: 4, md: 6 }}
        flexDir={{ base: "column", md: "row" }}
        justifyContent="space-between"
        maxW="container.lg"
        alignItems="center"
      >
        <Link as={RRLink} to="/">
          <Logo />
        </Link>

        <Box w="xs">
          <Search />
        </Box>
      </Container>
    </Flex>
  );
};
