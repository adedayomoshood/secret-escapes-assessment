import { CircularProgress, Flex } from "@chakra-ui/react";

export const Loader = () => {
  return (
    <Flex
      top={0}
      left={0}
      w="100vw"
      h="100vh"
      pos="fixed"
      zIndex={9}
      bg="blackAlpha.400"
      alignItems="center"
      justifyContent="center"
    >
      <CircularProgress size="100px" color="orange" thickness={2} isIndeterminate />
    </Flex>
  );
};
