import {
  Button,
  Flex,
  FormControl,
  Icon,
  IconProps,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement
} from "@chakra-ui/react";
import { FormEvent, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

export const Search = ({ isLarge }: { isLarge?: boolean }) => {
  const navigate = useNavigate();
  let [searchParams] = useSearchParams();
  const query = searchParams.get("query") || "";

  const [queryParam, setQueryParam] = useState(query);

  const handleSearch = (e: FormEvent) => {
    e.preventDefault();

    if (queryParam !== query) navigate(`/search?query=${queryParam}`);
  };

  return (
    <Flex as="form" justifyContent="center" onSubmit={handleSearch}>
      <FormControl>
        <InputGroup size={isLarge ? "lg" : "md"} bg="white">
          <InputLeftElement pointerEvents="none" children={<SearchIcon />} />

          <Input
            required
            borderRadius={0}
            placeholder="Enter location"
            defaultValue={queryParam || query}
            onChange={(e) => setQueryParam(e.target.value)}
          />

          <InputRightElement w={isLarge ? 32 : 24}>
            <Button
              w="full"
              h="full"
              colorScheme="orange"
              borderRadius={0}
              type="submit"
            >
              Search
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>
    </Flex>
  );
};

export const SearchIcon = ({ fill = "gray.400" }: IconProps) => (
  <Icon viewBox="0 0 18 18" w={4} h={4} fill={fill}>
    <path d="m14.294 12.003 3.215 3.215c.655.655.655 1.637 0 2.291-.327.327-.818.491-1.145.491-.328 0-.819-.164-1.146-.49l-3.215-3.216a7.773 7.773 0 1 1 2.29-2.29Zm-6.521.76a4.99 4.99 0 1 0 0-9.981 4.99 4.99 0 0 0 0 9.982Z" />
  </Icon>
);
