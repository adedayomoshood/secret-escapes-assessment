import { Flex } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { Header } from "../Header";

export const Layout = () => {
  return (
    <Flex gap={6} flexDir="column">
      <Header />

      <Outlet />
    </Flex>
  );
};
