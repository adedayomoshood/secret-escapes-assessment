import { useQuery } from "@apollo/client";
import { SALES_DETAILS } from "./../graphql";

export const useSaleDetails = (saleId: string) => {
  const { loading, error, data } = useQuery(SALES_DETAILS, {
    variables: {
      saleId,
    },
  });

  return {
    saleDetailError: error?.message,
    saleDetailLoading: loading,
    saleDetail: data?.sale,
  };
};
