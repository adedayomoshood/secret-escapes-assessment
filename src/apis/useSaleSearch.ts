import { useLazyQuery } from "@apollo/client";
import { useState } from "react";
import { SaleSearchQuery } from "../__generated__/graphql";
import { SALES_SEARCH } from "../graphql";

export const useSaleSearch = () => {
  const [result, setResult] = useState<SaleSearchQuery>();

  const [search, { loading, error, fetchMore }] = useLazyQuery(SALES_SEARCH, {
    variables: {
      limit: 10,
      query:''
    },
    notifyOnNetworkStatusChange: true,
    onCompleted: (data) => {
      setResult(data);
    },
  });

  return {
    search,
    saleSearchLoading: loading,
    saleSearchError: error?.message,
    fetchMoreDestinations: fetchMore,
    destinations: result?.saleSearch?.sales,
    destinationsCount: result?.saleSearch?.resultCount,
  };
};
