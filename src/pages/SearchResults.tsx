import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Container,
  Flex,
  Heading,
  SimpleGrid,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { useSaleSearch } from "../apis";
import { Loader } from "../components/Loader";
import { SearchResultDestination } from "../components/SearchResultDestination";

const SearchResults = () => {
  let [searchParams] = useSearchParams();
  const [error, setError] = useState("");
  const query = searchParams.get("query");

  const {
    search,
    destinations,
    saleSearchError,
    saleSearchLoading,
    destinationsCount,
    fetchMoreDestinations,
  } = useSaleSearch();

  useEffect(() => {
    setError("");

    if (query) {
      search({ variables: { query } });
    } else setError("Please, enter a location to get destinations.");
  }, [query, search]);

  const fetchMore = () => {
    fetchMoreDestinations({
      variables: { offset: destinations?.length },

      updateQuery: (existingData, { fetchMoreResult }) => {
        if (!fetchMoreResult.saleSearch) {
          return existingData;
        }

        const existingDataSales = existingData.saleSearch?.sales || [];
        const fetchMoreSales = fetchMoreResult.saleSearch?.sales || [];

        fetchMoreResult.saleSearch.sales = [
          ...existingDataSales,
          ...fetchMoreSales,
        ];

        return { ...fetchMoreResult };
      },
    });
  };

  return (
    <Container maxW="container.lg" as={Flex} flexDir="column" gap={8} pb={10}>
      {error || saleSearchError ? (
        <Alert status="error">
          <AlertIcon />
          {saleSearchError || error}
        </Alert>
      ) : null}

      {!saleSearchLoading && destinations?.length === 0 ? (
        <Alert status="warning">
          <AlertIcon />
          No destination for this location. Please, search for another location.
        </Alert>
      ) : null}

      {saleSearchLoading && <Loader />}

      {destinationsCount ? (
        <Heading>
          {destinationsCount}{" "}
          <Box as="span" fontWeight="normal">
            destinations
          </Box>
        </Heading>
      ) : null}

      {destinations ? (
        <>
          <SimpleGrid columns={{ base: 1, md: 2 }} spacing={8}>
            {destinations?.map((item) => (
              <SearchResultDestination
                key={item?.id}
                title={item?.editorial?.title || ""}
                destination={item?.editorial?.destinationName || ""}
                LeadPhotoUrl={item?.photos?.[0]?.url || ""}
                id={item?.id || ""}
              />
            ))}
          </SimpleGrid>

          {destinationsCount && destinationsCount > destinations?.length ? (
            <Box mx="auto">
              <Button
                size="lg"
                bg="black"
                borderRadius={0}
                variant="solid"
                colorScheme="blackAlpha"
                onClick={fetchMore}
              >
                Fetch more destinations
              </Button>
            </Box>
          ) : null}
        </>
      ) : null}
    </Container>
  );
};
export default SearchResults;
