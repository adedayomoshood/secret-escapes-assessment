import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Container,
  Flex,
  Heading,
  Image,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { Prose } from "@nikolovlazar/chakra-ui-prose";
import parse from "html-react-parser";
import { Fragment } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSaleDetails } from "../apis";
import { Loader } from "../components/Loader";

const SaleDetails = () => {
  const params = useParams();
  const navigate = useNavigate();

  const { saleDetailLoading, saleDetailError, saleDetail } = useSaleDetails(
    params?.saleId || ""
  );

  const backToResults = () => {
    navigate(-1);
  };

  return (
    <>
      {saleDetailLoading && <Loader />}

      {saleDetailError && (
        <Container maxW="container.lg">
          <Alert status="error">
            <AlertIcon />
            {saleDetailError}
          </Alert>
        </Container>
      )}

      {saleDetail && (
        <Fragment>
          <Flex flexDir="column" py={4} bg="white">
            <Container as={Flex} flexDir="column" maxW="container.lg" gap={4}>
              <Box>
                <Button
                  onClick={backToResults}
                  fontWeight="normal"
                  variant="link"
                >
                  Back to results
                </Button>
              </Box>

              <Heading as="h1" maxW="3xl">
                {saleDetail?.editorial?.title}
              </Heading>
              <Text fontSize="xl">
                {saleDetail?.editorial?.destinationName}
              </Text>

              <Heading as="h3" color="orange.400">
                {saleDetail?.prices?.leadRate?.forDisplay}
              </Heading>
            </Container>
          </Flex>

          <Container maxW="container.lg">
            <Box boxShadow="0 2px 5px hsl(0deg 0% 0% / 15%)">
              <Flex bg="black" alignItems="center" justifyContent="center">
                <Image
                  alt={
                    saleDetail?.editorial?.title ||
                    saleDetail?.editorial?.destinationName ||
                    ""
                  }
                  loading="lazy"
                  src={saleDetail?.photos?.[0]?.url || ""}
                  objectFit="cover"
                  w="100%"
                  h="400px"
                />
              </Flex>

              <Box p={{ base: 4, lg: 8 }} mx="auto" bg="white">
                <Prose>
                  {parse(saleDetail?.editorial?.hotelDetails || "")}
                </Prose>
              </Box>
            </Box>
          </Container>

          <SimpleGrid mt={8} columns={{ base: 1, md: 2, lg: 4 }} gap={4}>
            {saleDetail?.photos?.map((picture, index) => (
              <Image
                key={`key-${picture?.url}`}
                src={picture?.url || ""}
                alt={
                  saleDetail?.editorial?.title ||
                  saleDetail?.editorial?.destinationName ||
                  ""
                }
              />
            ))}
          </SimpleGrid>
        </Fragment>
      )}
    </>
  );
};

export default SaleDetails;
