import { Flex, Stack } from "@chakra-ui/react";
import { Logo } from "../components/Logo/Logo";
import { Search } from "../components/Search";

const HomePage = () => {
  return (
    <Flex h="100vh" w="100vw" alignItems="center" justifyContent="center">
      <Stack maxW="xl" flex={1} spacing={{ base: 6, lg: 10 }} p={4}>
        <Flex justifyContent="center">
          <Logo fill="black" />
        </Flex>

        <Search isLarge />
      </Stack>
    </Flex>
  );
};
export default HomePage;
