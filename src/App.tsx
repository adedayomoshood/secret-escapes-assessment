import { Route, Routes } from "react-router-dom";
import { Layout } from "./components/Layout";
import HomePage from "./pages/Homepage";
import SaleDetails from "./pages/SaleDetails";
import SearchResults from "./pages/SearchResults";

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />

      <Route path="/" element={<Layout />}>
        <Route path="/search" element={<SearchResults />} />

        <Route path="/sale/:saleId" element={<SaleDetails />} />

        <Route path="*" element={<HomePage />} />
      </Route>
    </Routes>
  );
}

export default App;
