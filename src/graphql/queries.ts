import { gql } from "../__generated__";

export const SALES_SEARCH = gql(/* GraphQL */ `
  query SaleSearch($query: String!, $offset: Int, $limit: Int) {
    saleSearch(query: $query, travelTypes: "HOTEL_ONLY") {
      resultCount
      sales(limit: $limit, offset: $offset) {
        id
        editorial {
          title
          destinationName
        }
        photos {
          url
        }
      }
    }
  }
`);

export const SALES_DETAILS = gql(/* GraphQL */ `
  query SaleDetails($saleId: String!) {
    sale(saleId: $saleId) {
      id
      editorial {
        title
        destinationName
        hotelDetails
      }
      prices {
        leadRate {
          forDisplay
        }
      }
      photos {
        url
      }
    }
  }
`);
