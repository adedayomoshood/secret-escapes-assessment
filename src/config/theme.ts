import { extendTheme } from "@chakra-ui/react";
import { withProse } from "@nikolovlazar/chakra-ui-prose";

const theme = extendTheme(
  {
    fonts: {
      heading: `'Source Sans Pro', sans-serif`,
      body: `'Source Sans Pro', sans-serif`,
    },
  },
  withProse()
);

export default theme;
