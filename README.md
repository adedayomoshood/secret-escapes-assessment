# Secret Escapes Assessment

A Front end engineering take home challenge from [Secret Escapes](https://www.secretescapes.com). The project is a web application that allows users to search for and view details of destinations.

## Project Details

**Live Demo:** - [https://secret-escapes.moshood.dev](https://secret-escapes.moshood.dev)

**Backend API:** - [https://staging.sparrow.escapes.tech/graphql/](https://staging.sparrow.escapes.tech/graphql/)

## Requirements

To get this application up and running. You need to have the following tools on your machine

- nodejs
- yarn or npm

## Starting the app

In order to start the aplication, you need to create a local copy of the env file with

    cp .env.example .env.local

Once the env file has been created, you should run the following code one after the other

    $ yarn
    $ yarn start

If the app starts successfully, you will see the following in your terminal

    Compiled successfully!

    You can now view secret-escapes-assessment in the browser.

      Local:            http://localhost:7300
      On Your Network:  http://***.***.***.***:7300

    Note that the development build is not optimized.
    To create a production build, use yarn build.

**Frontend Url:** `http://localhost:7300`

## Tools

This app was bootstraped with **create-react-app** and uses the following tools.

![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Apollo-GraphQL](https://img.shields.io/badge/-ApolloGraphQL-311C87?style=for-the-badge&logo=apollo-graphql)
![React Router](https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=react-router&logoColor=white)
![Chakra](https://img.shields.io/badge/chakra-%234ED1C5.svg?style=for-the-badge&logo=chakraui&logoColor=white)
